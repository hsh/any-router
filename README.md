# AnyRouter

[![CI Status](https://img.shields.io/travis/hushaohua8503/AnyRouter.svg?style=flat)](https://travis-ci.org/hushaohua8503/AnyRouter)
[![Version](https://img.shields.io/cocoapods/v/AnyRouter.svg?style=flat)](https://cocoapods.org/pods/AnyRouter)
[![License](https://img.shields.io/cocoapods/l/AnyRouter.svg?style=flat)](https://cocoapods.org/pods/AnyRouter)
[![Platform](https://img.shields.io/cocoapods/p/AnyRouter.svg?style=flat)](https://cocoapods.org/pods/AnyRouter)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

AnyRouter is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'AnyRouter'
```

## Author

hsh, qinyue0306@163.com

## License

AnyRouter is available under the MIT license. See the LICENSE file for more info.
